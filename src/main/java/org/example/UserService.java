package org.example;

public class UserService {

    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = new UserRepository();
    }

    public String fullName(String firstName, String lastName) {
        if (firstName == null || lastName == null)
            throw new IllegalArgumentException("Not null bratan");
        return firstName + " " + lastName;
    }

    public void doSth(Integer id) {
        if (id == null)
            throw new IllegalArgumentException();

        System.out.println("Deleted");
    }

    public String getName(Integer id) {
        if (id == null)
            throw new IllegalArgumentException();

        String res = userRepository.getFirstNameById(id);
        return res + "AAAAAA";
    }
}
