package org.example;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;

@TestMethodOrder(value = MethodOrderer.OrderAnnotation.class)
@ExtendWith(MockitoExtension.class)
public class UserServiceTest {

    UserService userService;

    UserRepository userRepository;

    @BeforeEach
    public void setUp() {
        userRepository = Mockito.mock(UserRepository.class);
        userService = new UserService(userRepository);
    }

    @DisplayName("Success")
    @Order(200)
    @Test
    public void testFullNameSuccess() {
        String result = userService.fullName("A", "B");
        String expected = "A B";
        assertEquals(expected, result);
    }

    @DisplayName("First")
    @Order(100)
    @Test
    public void testFullNameFailLastnameNull() {
        assertThrows(
                IllegalArgumentException.class,
                () -> userService.fullName("A", null)
        );
    }

    @DisplayName("Last")
    @Order(300)
    @Test
    public void testFullNameFailFirstnameNull() {
        IllegalArgumentException illegalArgumentException = assertThrows(
                IllegalArgumentException.class,
                () -> userService.fullName(null, "B")
        );
        System.out.println(illegalArgumentException.getMessage());
    }

    @Test
    public void testDoSthSuccess() {
        assertDoesNotThrow(() -> userService.doSth(5));
    }

    @Test
    public void testDoSthFail() {
        assertThrows(IllegalArgumentException.class, () -> userService.doSth(null));
    }


    @Test
    public void testGetNameSuccess() {

        Mockito.when(userRepository.getFirstNameById(any())).thenReturn("1 A");

        String result = userService.getName(2);

        assertEquals("1 A", result);
    }

    @Test
    public void testRepo() {
        UserRepository userRepository1 = new UserRepository();
        String result = userRepository1.getFirstNameById(5);
        assertEquals("4 A", result);
    }
}
