package org.example;

import java.util.LinkedList;
import java.util.List;

public class UserRepository {
    private final List<String> users;

    public UserRepository() {
        List<String> users = new LinkedList<>();
        for (int i = 0; i < 10; i++)
            users.add(i + " A");

        this.users = users;
    }


    public String getFirstNameById(Integer id) {
        return users.get(id - 1);
    }
}
